package bean;
//TESTING 
public class Node {
	private int value;
	private Node previousNode;
	private Node nextNode;

	public Node(int value) {
		super();
		this.value = value;
		this.previousNode = null;
		this.nextNode = null;
	}

	public Node(int value, Node previousNode, Node nextNode) {
		super();
		this.value = value;
		this.previousNode = previousNode;
		this.nextNode = nextNode;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getPreviousNode() {
		return previousNode;
	}

	public void setPreviousNode(Node previousNode) {
		this.previousNode = previousNode;
	}

	public Node getNextNode() {
		return nextNode;
	}

	public void setNextNode(Node nextNode) {
		this.nextNode = nextNode;
		if (nextNode != null) {
			nextNode.previousNode = this;
		}
	}
}
