package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import bean.Node;
import stack.Stack;

public class StackTest {
	private Stack stackEmpty;
	private Stack stack;
	private Node n1, n2;
	
	@Before
	public void init() {
		n1 = new Node(1, null, null);
		n2 = new Node(2, null, null);
		stackEmpty = new Stack();
		stack = new Stack(n1);
	}

	@Test
	public void testIsEmpty() {
		assertTrue(stackEmpty.isEmpty());
		assertFalse(stack.isEmpty());
	}

	@Test
	public void testTop() {
		n1.setNextNode(n2);
		assertEquals(n2.getValue(), stack.top());
	}

	@Test
	public void testElementAt() {
		stack.push(2);
		stack.push(3);
		
		assertEquals(stack.elementAt(2), 3);
		assertEquals(stack.elementAt(1), 2);
	}
	
	@Test
	public void testPush() {
		stackEmpty.push(1);
		stack.push(2);
		
		assertEquals(stackEmpty.getNbOfNodes(), 1);
		assertEquals(stackEmpty.top(), 1);
		
		assertEquals(stack.getNbOfNodes(), 2);
		assertEquals(stack.top(), 2);
	}
	
	@Test
	public void testPop() {
		stack.push(2);
		stack.pop();
		assertEquals(stack.getNbOfNodes(), 1);
	}
 

	@Test
	public void testClone() {
		stack.push(2);
		stack.push(3);
		stack.push(4);
		
		Stack stackCopy = stack.clone();
		assertEquals(stack.elementAt(0), stackCopy.elementAt(0));
		assertEquals(stack.elementAt(2), stackCopy.elementAt(2));
	}

}
