package stack;

import bean.Node;

public class Stack {
	Node firstNode;
	int nbOfNodes;

	public Stack() {
		firstNode = null;
		nbOfNodes = 0;
	}

	public Stack(Node node) {
		firstNode = node;
		nbOfNodes = 1;
	}

	public boolean isEmpty() {
		if (nbOfNodes == 0) {
			return true;
		}
		return false;
	}

	public int top() {
		int lastNodeValue = -1;
		lastNodeValue = getLastNode().getValue();
		return lastNodeValue;
	}

	public void push(int element) {
		if(nbOfNodes > 0){
			Node lastNode = getLastNode();
			Node newNode = new Node(element, lastNode, null);
			lastNode.setNextNode(newNode);
			lastNode = newNode;
		}
		else{
			firstNode = new Node(element, null, null);
		}
		nbOfNodes ++;
	}

	public void pop() {
		Node lastNode = getLastNode();
		if(lastNode.getPreviousNode() != null){
			Node newLastNode = lastNode.getPreviousNode();
			newLastNode.setNextNode(null);
		}
		nbOfNodes --;
	}

	public int elementAt(int position) {
		int valueAt = -1;
		int index = 0;
		for(Node n = firstNode; n != null; n = n.getNextNode()){
			if(index == position){
				valueAt = n.getValue();
				break;
			}
			index ++;
		}
		return valueAt;
	}

	public Stack clone() {
		Stack newStack = new Stack();
		for(int i = 0; i < nbOfNodes; i++){
			System.out.println(i + " | " + this.elementAt(i));
			newStack.push(this.elementAt(i));
		}
		return newStack;
	}

	public int getNbOfNodes() { 
		return nbOfNodes;
	}
	
	public Node getLastNode(){
		Node foundNode = null;
		for (Node n = firstNode; n != null; n = n.getNextNode()) {
			foundNode = n;
		}
		return foundNode;
	}

}
